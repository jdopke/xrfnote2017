\documentclass[final,a4paper]{article}

\pdfoutput=1

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{abstract}       % Allows twocolumn abtract
\usepackage{authblk}
\usepackage{float}
\usepackage[english]{babel}
\usepackage{graphicx,subcaption}
\usepackage{todonotes}
\usepackage{lineno}

\linenumbers

\title{Towards Dark Field X-ray Tomography to Reveal Ancient Writing}

\author[1,*]{Jens Dopke}
\author[2]{Michael Drakopolous}
\author[3]{Alexy Karenowska}
\author[3, 4]{Maksymilian Marzec}
\author[3]{Dirk Obbink}
\author[5]{Seth Parker}
\author[5]{Brent Seales}
\author[1]{Matthew Veale}
\author[1]{Matthew Wilson}

\affil[1]{STFC Rutherford Appleton Laboratory}
\affil[2]{Diamond Light Source Ltd.}
\affil[3]{University of Oxford}
\affil[4]{Pembroke College, Oxford}
\affil[5]{University of Kentucky}

\affil[*]{jens.dopke@stfc.ac.uk}

\begin{document}
%  \twocolumn[
    \maketitle             % full width title
    \begin{onecolabstract} % ditto abstract
      This paper introduces the use of field x-ray tomography, based on K-line X-ray fluorescence, to reveal ancient writing in carbon-based ink. It particularly seeks to demonstrate how trace elements within the ink found in Herculaneum Scrolls can be used to reveal ancient writing that are otherwise hard to differentiate from the  papyrus on which it is written. We show that Pb K-Line fluorescence can be stimulated and observed in several subjects. A first attempt at imaging a flat original subject as well as establishing the concentration of lead in the ink is presented in this paper.
    \end{onecolabstract}
 % ]

  \section{Introduction}

    The Herculaneum Scrolls are a collection of more than 1,800 rolled documents. They were discovered in the southern  Italian archaeological site of Herculaneum in 1750, in a building that has become known as the Villa of the Papyri. The Villa is the only complete ancient library ever to have been discovered and an important source of insight into the recorded history of the ancient world. The contents of the library was carbonized in the eruption of Mount Vesuvius in AD 79 and buried under layers of inert cement-like debris. This process has left the papyri intact and well-preserved but in an extremely fragile state: the carbonisation make them brittle and prevents them from being unrolled without destroying them. Before the delicate state of the documents was recognised to make it impossible to non-destructively access their contents, numberous attempts were made and some insight was gained into the nature of their contents. However, modern preservationists have long since recognised that the only viable means to access the texts is one which can be applied to the scrolls in their entirety - i.e. without physically unrolling them.\todo{Some reference(s) here? Check with Alexy} 

		Previous attempts to image the Herculaneum papyri have used x-ray Computed Tomography (CT) to provide some insight into the structure of the scrolls\cite{Seales}.  However, this through-imaging approach has had very limited success in discerning the texts themselves on account of the low contrast provided by the carbon-based ink. Another approach using Phase Contrast Computed Tomography has shown more promising results\cite{PCCT_MOCELLA} but tests with a bench-top based phase-contrast system have yet to show an equivalent performance limiting the extent to which this technique can be practically applied to the large quantities of material available. Finally, after initial finds of trace concentrations of lead within the ink\cite{Seales}, an approach was made to image the surface using surface X-ray Fluorescence (XRF) up to Pb L-lines\cite{Brun3751}. However, the low-energetic nature of Pb M- and L-lines does not allow to penetrate into a scroll and will therefore not deliver images of the inside of an intact scroll.

    Using the existence of lead traces within the ink of herculaneum papyri\cite{Seales, Brun3751} we developed the idea of setting up an XRF CT, to scan for ink, whilst also imaging the topography of the scrolls. XRF measurements to date have been performed using L- or M-lines, whilst in this paper we discuss the use of K-line fluorescence and first tests thereof, as a means of imaging the central parts of a scroll.

  \section{Methodology}

    \begin{figure}[htbp]
      \centering
        \includegraphics[width=0.95\textwidth]{figures/SchematicView}
      \caption{A schematic view of the imaging operation. Whilst depicting a 90$^\circ$ setup, the angle of observation can be varied to generate a different resulting spectrum.}
      \label{fig:SchematicView}
    \end{figure}
		
		To create contrast within the ink filled regions of the papyrus, we suggest using dark-field x-ray fluorescence imaging combined with an energy resolved sensor (see Figure \ref{fig:SchematicView}): rather than observing x-rays that penetrated through the object, the idea is to measure x-rays originating from the objects, to identify fluorescence within the object. By examining a range of fluorescent x-ray energies of known observed trace materials in the ink, one can then increase contrast for the writing itself.

    To enable tomographic reconstruction of writing from inside an intact multi-layer scroll, fluorescent x-rays are required to create enough contrast to resolve the ink but must also be generated at a high enough energy to prevent re-absorption and minimize scattering within the subject.  Given the relatively high observed fraction of lead in herculaneum ink (between 10-100\,$\mu g cm^{-2}$\cite{Seales, Brun3751}\footnote{Whilst \cite{Seales} has established the presence of a large fraction of lead in the writing, a surface concentration has only been measured by \cite{Brun3751}}) we are looking for x-ray emission in Pb K-lines above 70\,keV energy.
		
		\subsection{Detector}
		
		Efficient absorption of high energetic x-ray photons within a sensor whilst retaining high lateral resolution requires a sensor based on high-z materials. In our case we chose to operate with a Hexitec\cite{HEXITEC} detector based on a 1\,mm thick CdTe sensor. This sensor absorbs more than 70\,\% of the incident x-rays at <90\,keV. Hexitec is segmented into 80x80 square pixels with 250\,$\mu m$ height. For each frame recorded by Hexitec (up to about 10k Frames/s), each of the pixels reports the maximum energy observed since the last readout. Frames are written out as an array of unsigned 16\,bit values, and a calibration (gradient and intersect) is applied to derive measured x-ray energies. A spectrum per pixel can be obtained by recording many frames and generating a histogram of all hits above a minimum threshold (typically 1.5-3\,keV). Clustering of hits observed in neighbouring pixels can be applied or explicitly rejected, affecting efficiency and energy resolution.

    Once the k-line emission has been imaged to detect the ink, the Compton scattering for the same dataset can be imgaged as well. This approach potentially allows the reconstruction of both the ink (k-line data) and the papyrus layers (Compton data) from a single observation of the subject. These are the two key ingredients to allow the virtual unwrapping\cite{Unwrapping} and textural reconstruction of the scroll.

    \subsection{Background}

      The origin of the detected background in measuring k-line emissions is largely Compton scattering; two different approaches can be used to reduce its effect on the measurement:
    
      \begin{itemize}
        \item Observing the sample from a direction perpendicular to the angle of incidence of a highly polarised x-ray beam (such as that produced at Diamond Light Source Ltd.).
        \item Using a monochromatic beam set just above the k-edge for lead, a high scattering angle (around 150 degrees) can be used. This has the effect of shifting the Compton peak energy well below the lowest k$_{alpha}$ emission energy of lead, thus increasing the signal to background ratio significantly. This method can also be used for a bench-top setup, based on a (sizeable) 109-Cd source that emits a small fraction of x-rays with an energy of 88.036\,keV, $\sim$25.5\,eV above the Pb k-edge.
      \end{itemize}
			
			Most of the data recorded at Diamond's beamline I12 in Summer 2017 was taken in the former configuration and hence the discussion herein focuses on that approach.

    \subsection{Setup}

      Based on preliminary tests done in August 2016, we attempted to remove all possible external background. To reduce the presence of X-rays not coming in through our measurement pinhole, we wrapped our detector as well as the flight path between detector and pinhole with 3\,mm thick tungsten tubing. Additionally, the incoming and outgoing x-rays were enclosed in flight tubes to reduce the air-scatter hitting the detector shielding.

      \begin{figure}[htbp]
        \centering
          \includegraphics[width=0.95\textwidth]{figures/Setup105SideView}
        \caption{A view of the setup, focussing on the sample holder. Visible are the incoming and outgoing flight tubes (left and right) as well as the Pinhole mount behind the sample.}
        \label{fig:Setup105SideView}
      \end{figure}

      Distances were chosen to maximize the detector area used and to achieve as high an efficiency as possible. The distance between the centre of the sample (the rotational axis of the system) and the pinhole was 35\,mm and a magnification of 5 was used. A 2\,mm thick tungsten pin-hole of 400\,$\mu m$ diameter was used during normal operation with a single attempt at 200\,$\mu m$ diameter to observe resolution improvement and efficiency loss. A photo of the setup at Diamond's beamline I12 is visible in Figure \ref{fig:Setup105SideView}.
		
      In perpendicular observation the incident x-ray energy was set to be 105\,keV, a compromise between high enough energy to excite the k-lines and not overlap the Compton photon energy with the k$_\alpha$ emissions, whilst not reducing incident flux too much. This did however remove any sensitivity to k$_{\beta}$ emissions.

    \subsection{Samples}

      To determine the concentrations of lead present within real objects as well as to establish sensitivity parameters of the aparatus, various phantoms were created using new papyrus and ink comprising activated carbon powder, distilled water, gum arabic and varying quantities of lead nitrate.

  		\begin{figure}[htbp]
				\centering
				\begin{subfigure}[b]{0.45\textwidth}
					\centering
						\includegraphics[width=0.95\hsize]{figures/ExampleSlide}
					\caption{An exemplary film holder supporting a phantom. Ink deposition on this particular phantom shows trials of uniform areal deposition of ink with known lead concentrations.}
					\label{fig:ExampleSlide}
				\end{subfigure}
				\hfil
				\begin{subfigure}[b]{0.45\textwidth}
					\centering
						\includegraphics[width=0.95\hsize]{figures/SampleInHolder}
					\caption{The test subject (black, left hand inside the film holder) within its support at the Diamond beamline I12. The sample was supported in a pre-folded 50\,um Polyimide film, clamped into a film holder.}
					\label{fig:SampleInHolder}
				\end{subfigure}
					\caption{Examplary samples used in this paper. Film holders were a simple way of getting all samples held stably in the beam without adding large amounts of material.}
					\label{fig:SampleFigures}
			\end{figure}

			Several trials of deposition using writing tools showed that measured surface concentrations of lead (through XRF observation) were highly dependent on ink flow. However, it was found that drop deposition  using a pipette allowed a reasonably good approximation of a uniform lead concentration. Accordingly, a grid of ink samples was created on a small rectangle of papyrus comprising an array of 2x2\,mm squares, each containing a precisely pipetted 1$\pm$0.05\,$\mu l$ of lead doped ink solution(c.f. Figure \ref{fig:ExampleSlide}).\todo{Alexy for a Photo of the actual slide that was used!} 

			\begin{table}
				\centering
				\caption{Concentration phantoms, their estimated surface concentration of lead and illumination times.}
				\label{tab:Concentrations}
				\begin{tabular}{l c c c c c c}
					Name & Factor  & Estim. Conc. & Illum. Time\\
					& wrt. Delta & [$\mu g/cm^2$] & [s] \\ \hline
					Alpha & 1000 & 7800 & 7.2 \\
					Beta & 100 & 780 & 72 \\
					Beta/3 & 33 & 230 & 240 \\
					Gamma & 10 & 78 & 720 \\
					Gamma/3 & 3.3 & 23 & 2400 \\
					Delta & 1 & 7.8 & 7200
				\end{tabular}
			\end{table}

			All values in Table \ref{tab:Concentrations}\todo{Alexy thinks she can provide error bars, but do we actually care? Discuss!} are as per measurements made during mixture and dilution of our own ink. Care was taken to generate a homogenous mixture; however, it should be recognised that, in case of low lead concentrations, the deposited concentrations may differ from the values above.

			In addition to concentration phantoms for ink detection efficiency measurements, we also fabricated phantoms to establish an understanding of character detection resolution, as well as a multi-layered phantom to measure parallax shift and attempt CT reconstruction. For ease of use all the latter phantoms were set up with beta concentration levels, reducing the illumination time required and therefore providing more time for sensitivity studies and authentic sample measurements. There is much to discuss and much scope for further study in this area but this paper will focus solely on concentration measurements and approaches for reducing background.

			Real sample measurements were performed on the sample used in \cite{Seales, MLA}, a single lunate sigma on a single layer section of 3\,mm diameter herculaneum papyrus, a fragment of a Parisian scroll, first scanned in 2012\footnote{A broken of single letter piece of PHerc Paris 2, Fragment 96}. The sample in its support is shown in Figure \ref{fig:SampleInHolder}.

		\subsection{Calibration}
			Before performing measurements at the beamline, the setup was calibrated for its geometry as well as energy resolution. This was provided through the beamline and detector teams, and therefore is not part of this paper. It was found during the later analysis, that an active recalibration is required during long operation of the detector to increase energy resolution and particularly to improve inter-calibration of pixels in the sensitive energy range.
		

	\section{Results}


		\subsection{Concentration Phantoms}

    \begin{figure}[htbp]
      \centering
        \includegraphics[width=0.95\textwidth]{figures/CountSpectrumSquares}
      \caption{Per-pixel count rates per second within the k-line energy range for different concentration samples as per table \ref{tab:Concentrations}.}
      \label{fig:CountSpectrumSquares}
    \end{figure}

      In a first measurement we used the concentration phantoms to establish sensitivity of the measurement apparatus. Each phantom was illuminated for a time equivalent to the inverse of the estimated lead concentration within the square (see last column of table \ref{tab:Concentrations}). Measuring a response per pixel, we derive a histogram for all pixels covered with ink, estimating the counts per second and pixel from k-line fluorescence, as well as the spread in concentration. The resulting plot in Figure \ref{fig:CountSpectrumSquares} shows a variation of count rates over an expected range of three orders of magnitude.
      
		\begin{figure}[htbp]
    \begin{subfigure}[b]{0.30\textwidth}
      \centering
        \includegraphics[width=0.95\hsize]{figures/alpha_fit}
      \caption{Alpha}
      \label{fig:AlphaFit}
    \end{subfigure}
		\hfil
    \begin{subfigure}[b]{0.30\textwidth}
      \centering
        \includegraphics[width=0.95\hsize]{figures/beta_fit}
      \caption{Beta}
      \label{fig:BetaFit}
    \end{subfigure}
		\hfil
    \begin{subfigure}[b]{0.30\textwidth}
      \centering
        \includegraphics[width=0.95\hsize]{figures/beta3_fit}
      \caption{Beta$/$3}
      \label{fig:Beta3Fit}
    \end{subfigure}
		\vspace{0.5cm}
    \begin{subfigure}[b]{0.30\textwidth}
      \centering
        \includegraphics[width=0.95\hsize]{figures/gamma_fit}
      \caption{Gamma}
      \label{fig:GammaFit}
    \end{subfigure}
		\hfil
    \begin{subfigure}[b]{0.30\textwidth}
      \centering
        \includegraphics[width=0.95\hsize]{figures/gamma3_fit}
      \caption{Gamma$/$3}
      \label{fig:Gamma3Fit}
    \end{subfigure}
		\hfil
    \begin{subfigure}[b]{0.30\textwidth}
      \centering
        \includegraphics[width=0.95\hsize]{figures/delta_fit}
      \caption{Delta}
      \label{fig:DeltaFit}
    \end{subfigure}
      \caption{Resulting spectra in the k$_\alpha$ region for all concentration phantoms. Background was estimated and subtracted using a 3rd order polynomial fit. Peak counts fluctuate but remain in the estimated constant order of magnitude. The background visibly affects the spectrum as concentrations decrease, see particularly in Gamma$/$3 and Delta samples.}
      \label{fig:ConcentrationFits}
		\end{figure}

      In addition to estimating count rates from these phantoms, our first set of measurements also allowed us to understand how much the x-ray background would affect the signal extraction. As recording time was adjusted to the signal concentration, recorded peaks across the different samples looked very much alike, whilst the background slowly took over as we went from high to low concentrations. This is visible in Figure \ref{fig:ConcentrationFits} as the upper half of these plots is purely background and slowly develops larger variability as we go to lower concentrations.

    \subsection{Background Removal}

      In an initial trial, processed data from hexitec was used for the analysis and we attempted to remove background based on a simple 3rd order polynomial fit outside the sensitive region of the spectrum. Per-pixel statistics were found to be insufficient so we attempted to sum up spectra from adjacent pixels: this has the effect of boosting the performance of the fit but effectively decreasing lateral resolution. However, due to bad inter-calibration of pixels we observed a decrease in energy resolution. We therefore returned to the raw detector data, performing calibration based on known samples (high concentration lead and gold samples) as well as the Compton peak observed in all measurements.

			\begin{figure}[htbp]
				\centering
					\includegraphics[width=0.95\textwidth]{figures/45_48_fit}
				\caption{A single pixel spectrum and fits performed: red shows the raw histogram, yellow is a Compton peak fit derived from \cite{compton}, light blue is an additional linear tail, to adapt to the level of the spectrum below 72\,keV, turquoise is a combined k$_\alpha$ and k$_{\beta_{1,3}}$ fit, performed on the reduced spectrum and green is the difference between the fit line (dark blue) and the raw spectrum.}
				\label{fig:fitresult}
			\end{figure}

			To create an appropriate background model dominated by Compton scattering we applied a fit to the Compton peak including exponential tails, derived from a description in \cite{compton}. Adding a further linear tail to that fit, allowed us not only to describe the background very well, but also to constrain the energy scale sufficiently to allow combination of adjacent pixels, increasing statistics for the k-line emission spectrum to be fitted. A representative fit with individual components is shown in Figure \ref{fig:fitresult}.

		\subsection{Herculaneum Sample}

			\begin{figure}[htbp]
				\centering
					\begin{subfigure}[b]{0.90\textwidth}
						\centering
							\includegraphics[width=0.5\hsize]{figures/Fragment}
						\caption{A close up photograph of the single-letter fragment of Herculaneum material, also used in \cite{Seales, MLA}. The approximate diameter of the sample is 3\,mm.}
						\label{fig:Fragment}
					\end{subfigure}
				\vspace{0.5cm}
				\centering
					\begin{subfigure}[b]{0.90\textwidth}
						\centering
							\includegraphics[width=0.90\hsize]{figures/SampleRecoloured}
						\caption{Fit result for k$_{\alpha}$ peak area (arbitrary scale) as measured above background in a 90$^\circ$ setup with 400\,$\mu m$ pin-hole. The physical outline of the sample is still visible, with a clear indication of where lead was observed. The sample is observed under 45$^\circ$ angle and no perspective correction has been performed, thus the horizontal dimension is shrunk with respect the the actual sample in \ref{fig:Fragment}. A part of the sample surface had come off during operation and appears as a bright dot on the bottom left, as it does contain large fractions of lead.}
						\label{fig:SampleMeasurement}
					\end{subfigure}
	      \caption{Photographic display of the sample as well as the measured lead concentration (arbitrary scale) in k-line emissions.}
	      \label{fig:FragmentPlusMeasurement}
			\end{figure}
			
			Having run the previous calibrations as well as background estimations, we moved to the authentic Herculaneum fragment used in previous investigations\cite{Seales}, shown in Figure \ref{fig:Fragment}.

	    Given the known magnitude of lead concentration within that sample\cite{Seales} as well as other Herculaneum samples\cite{Brun3751}, we ran multiple overnight exposures with it, using 400\,$\mu m$ and 200\,$\mu m$ diameter pinholes for imaging. These exposures delivered enough statistics to allow us to conclude that the concentration level observed was in the range of our gamma$/$3 phantom. As can be seen in Figure \ref{fig:SampleMeasurement}, the shape of the sample is visible in k$_{\alpha}$ measurements (due to Compton scattering and therefore increased background uncertainty, see faint blue area). On top of the shape itself, there is a higher number of counts observed where the writing is visible. It is recognised that the sample has suffered from being mechanically handled and we therefore consider the non-complete overlap of the observed k$_{\alpha}$ emissions with the writing an artefact of the sample having changed throughout handling.

			Measurements on the Herculaneum fragment were repeated at a large scattering angle. However, given the limited time available to make our measurements, this did not allow us to establish the equivalent sensitivity to the 90$^\circ$ configuration as the setup had to be reconfigured for the overnight data gathering. It is however noticeable that the Compton background is significantly reduced in this arrangement by reducing the incident photon energy to about 89\,keV and thereby, in combination with the larger scattering angle, reducing the energy of the Compton peak to about 67\,keV below the Pb k$_{\alpha}$ emissions.

  \section{Conclusion and Outlook}

    In this paper we have described the first attempts to implement dark-field x-ray tomography using k-line X-ray fluorescence for the purpose of detecting and analysing samples of organic (i.e. carbon-based) ancient ink. Our work has demonstrated that this technique has important promise in the context of allowing access to texts that are currently inaccessible to the world, such as the Herculaneum scrolls.

    We showed that writing made in ink with trace concentrations of lead can be made visible but - to be practical for measurements on whole scrolls - the methodology needs improvement in terms of signal to background ratio to allow quicker recording of data. Refinement of the technique using a synchroton light source will be carried out, provided future beamtime can be secured. The authors also intend to build a lab-based setup to allow us continued improvement of the methodology without synchrotron access.

\bibliography{Diamond2017} 
\bibliographystyle{ieeetr}

  \section{Acknowledgements}

		We acknowledge Diamond Light Source for time on beamline I12 under proposal EE15795 and we are grateful for the support coming from all of I12 staff. Work on the Data analysis was supported by STFC Particle Physics Department as well as a Rokos Awards Internship, awarded through Pembroke College, Oxford. The single-letter-fragment of Herculaneum scroll, Paris 4, Fragement 56, analysed herein was kindly provided to W.B. Seales for studies by the Institute de France.

	\section{Author Contributions}

		J. Dopke conceived the experiment, drove the realisation in finding material and effort, as well as analysing the data. M. Marzec worked as a summer student, supporting the re-analysis and providing a better description for Compton peak fitting. A. Karenowska and students provided phantoms for sensitivity measurements as well as all other phantoms not described in this article. M. Drakopolous  provided support for operating the beamline and pre-experimental discussions. M. Veale and M. Wilson provided the detector, calibrating the initial system and providing valuable input in discussion of the data. S. Parker and W.B. Seales got J. Dopke involved in the subject, seeding this experiment and provided the sample given to W.B. Seales by the Institute de France. D. Obbink provided contextual input. Most authors supported operation of the experiment during beamtime.


\listoftodos

\end{document}

















